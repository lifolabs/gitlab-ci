
# Gitlab-CI Deep Dive

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is Gitlab-CI ?
- Who needs Gitlab-CI ?
- How Gitlab-CI works ?
- How to manage Ansible in various environments and scenarios?
- What is Yaml ?
- What are Pipelines and Jobs?
- How can use write and use Jobs and Pipelines for our use?


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of CI with Gitlab.
- For junior/senior developers/operations who need knowledge in automated deployments.
- DevOps who wish to migrate from other CI tools to Gitlab-CI. 

---

# Course Topics

- Introduction to CI with Gitlab-CI.
  - DevOps culture and methodology
  - Concepts and definitions
  - Gitlab-CI position in big picture
- Automated DevOps pipelines
  - Overview
  - Automated DevOps with Gitlab-Ci
  - Integrating with k8s
  - DevOps with zero configuration
  - Creating config for Gitlab-CI
  - Building
- Build stages, artifacts and variables
  - Build stages
  - Parallel run in stages
  - Cache in Build
  - Defining Artifacts
  - Artifact re-use
  - passing variables to builds
- Automated app deployment
  - declaring deployment environment
  - k8s app resources
  - deploying an application to k8s
  - deploy token
  - dynamic env and review apps
- App quality and monitoring
  - integration and functional testing
  - analyzing  code quality 
  - application metrics
- Custom CI
  - launching dedicated running
  - automated runner
  - autoscaling cloud resources
  - custom k8s runners




---
# Pre Requisites
- Use of UNIX/Linux terminal
- Software development knowledge
  - Python and scripting would be sufficient
- Version control with git
- Understanding of containers and container management systems.
