# Gitlab-CI Deep Dive


.footer: Created By Alex M. Schapelle, VAioLabs.io

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is Gitlab-CI ?
- Who needs Gitlab-CI ?
- How Gitlab-CI works ?
- How to manage Ansible in various environments and scenarios?
- What is Yaml ?
- What are Pipelines and Jobs?
- How can use write and use Jobs and Pipelines for our use?


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of CI with Gitlab.
- For junior/senior developers/operations who need knowledge in automated deployments.
- DevOps who wish to migrate from other CI tools to Gitlab-CI. 

---

# Course Topics

- Introduction to CI with Gitlab-CI.
- Automated DevOps pipelines
- Build stages, artifacts and variables
- Automated app deployment
- App quality and monitoring
- Custom CI


---
# About Me
<img src="99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- over 12 years of IT industry Experience.
- fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - between each semester, I tried to take IT course at various places.
        - yes, one of them was A+.
        - yes, one of them was cisco.
        - yes, one of them was RedHat course.
        - yes, one of them was LPIC1 and Shell scripting.
        - no, others i learned alone.
        - no, not maintaining debian packages any more.
---

# About Me (cont.)
- over 7 years of sysadmin:
    - shell scripting fanatic
    - python developer
    - js admirer
    - golang fallen
    - rust fan
- 5 years of working with devops
    - git supporter
    - vagrant enthusiast
    - ansible consultant
    - container believer
    - k8s user

you can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---
# About Gitlab-CI

GitLab Inc. is the open-core company that provides GitLab, the DevOps software that combines the ability to develop, secure, and operate software in a single application. open source software project was created by Ukrainian developer Dmitriy Zaporozhets and Dutch developer Sytse Sijbrandij.

Since its founding, GitLab Inc. has been centered around remote work.GitLab has an estimated 30 million registered users, with 1 million being active license users.

---
# About Gitlab-CI (cont.)

The GitLab software was originally written in the Ruby programming language, with some parts later rewritten in the Go programming language. Initially, it was a source code management solution to collaborate within a team on software development that evolved to an integrated solution covering the software development life cycle, and then to the whole DevOps life cycle. The current software technology used includes Go, Ruby on Rails, and Vue.js.

---
# About Gitlab-CI (cont.)

It follows an open-core development model where the core functionality is released under an open-source (MIT) license while the additional functionality such as code owners, multiple issue assignees, dependency scanning and insights are under a proprietary license

---
# History
