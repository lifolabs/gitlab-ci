#!/usr/bin/env bash

#####################################################33
#created by silent-mobius
#purpse: build script for generating html and pdf scripts
#version: 0.2.21
#####################################################33

. /etc/os-release
PROJECT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


main(){
    while getopts "bch" opt
    do
        case $opt in
            b)  seek_all_md_files
                convert_data
                ;;
            c) clean_up
                ;;
            h) _help
                ;;
            *) _help
                ;;
        esac
    done

}

_help() {
    echo '[?] Incorrect use'
    echo "[?] Please use $0 \"-b\" for build and \"-c\" for clean up"
    echo "[?] exmaple: $0 -c"
}

clean_up() {
    echo '[+] cleaning up the previous builds'
    if [ -e $PROJECT/build.md ];then
      rm -rf $PROJECT/build.md
    fi

    if [ -e $PROJECT/presentation.html ];then
        rm -rf $PROJECT/presentation.html
    fi
    
    if [ -e $PROJECT/presentation.pdf ];then
        rm -rf $PROJECT/presentation.pdf
    fi

    echo '---------------------------------'
    echo '[+] Cleanup ended successfully   '
    echo '---------------------------------'
}

seek_all_md_files() {
    clean_up
    echo '[+] building'
    find $PROJECT -name '*.md'|sort|xargs cat > build.md

    echo '---------------------------------'
    echo '[+] Generate ended successfully  '
    echo '---------------------------------'
}

convert_data() {
    if [ $ID == 'ubuntu' ];then
        modules="fenced_code,codehilite,extra,toc,smarty,sane_lists,meta,tables"
    else
        modules="fenced_code,codehilite,extra,toc,smarty,sane_lists,meta,md_in_html,tables"
    fi
    if [ -x /usr/bin/darkslide ];then
        darkslide -v  -t $PROJECT/99_misc/.simple/ -x ${modules} build.md
    else
        echo '[!] Dependcy missing: please install darkslide or python-landslide'
    fi
}



#######
# Main
#######
main "$@"
